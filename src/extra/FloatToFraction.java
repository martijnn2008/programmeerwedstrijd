package extra;

// DKP 2012
// CORRECT

import java.util.*;
import java.math.BigInteger;

public class FloatToFraction {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        for (int i = 0; i < num; i++) {
            solve(sc);
        }
    }

    public static void solve(Scanner sc) {
        char[] in = sc.next().toCharArray();
        String fixed = "";
        String seq = "";
        boolean isSeq = false;
        for (int i = 2; i < in.length; i++) {
            if (in[i] == '(') isSeq = true;
            else if (in[i] == ')') isSeq = false;
            else if (isSeq) seq += in[i];
            else fixed += in[i];
        }
        long a = 0, b = 0;
        if (fixed.length() > 0) a += Long.parseLong(fixed);
        if (seq.length() > 0) b += Long.parseLong(seq);
        String divA = "1";
        String divB = "";
        for (int i = 0; i < seq.length(); i++) {
            divB += "9";
        }
        for (int i = 0; i < fixed.length(); i++) {
            divA += "0";
            divB += "0";
        }
        long c = 0, d = 0;
        if (divA.length() > 0) c = Long.parseLong(divA);
        if (divB.length() > 0) d = Long.parseLong(divB);
        BigInteger bigA;
        BigInteger bigB;
        if (b > 0) {
            bigA = BigInteger.valueOf(a).multiply(BigInteger.valueOf(d)).add(BigInteger.valueOf(c).multiply(BigInteger.valueOf(b)));
            bigB = BigInteger.valueOf(c).multiply(BigInteger.valueOf(d));
        } else {
            bigB = BigInteger.valueOf(c);
            bigA = BigInteger.valueOf(a);
        }
        BigInteger biggcd = bigA.gcd(bigB);
        System.out.println(bigA.divide(biggcd) + "/" + bigB.divide(biggcd));
    }
}