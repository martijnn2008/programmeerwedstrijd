package urionlinejudge_com_br;

// 1124 - ACCEPTED

import java.util.*;

public class Elevator {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (sc.hasNext()) {
			int L = sc.nextInt();
			int C = sc.nextInt();
			int R1 = sc.nextInt();
			int R2 = sc.nextInt();
			if (L == 0 && C == 0 && R1 == 0 && R2 == 0)
				break;
			if (solve(L, C, R1, R2)) {
				System.out.println("S");
			} else {
				System.out.println("N");
			}
		}
		sc.close();
	}

	static boolean solve(int h, int w, int r1, int r2) {
		if (w < 2 * Math.max(r1, r2))
			return false;
		if (h < 2 * Math.max(r1, r2))
			return false;

		int x1 = r1;
		int y1 = r1;
		int x2 = w - r2;
		int y2 = h - r2;

		int A = x2 - x1;
		int B = y2 - y1;

		int C = r1 + r2;
		if (C * C <= A * A + B * B) {
			return true;
		}

		return false;
	}
}
