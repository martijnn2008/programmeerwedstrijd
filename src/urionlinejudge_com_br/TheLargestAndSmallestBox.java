package urionlinejudge_com_br;

// 1294 - ACCEPTED

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TheLargestAndSmallestBox {
	public static void main(String[] args) throws Exception {
		InputStreamReader ir = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(ir);
		String line;
		while ((line = in.readLine()) != null) {
			solve(line);
		}
		ir.close();
		in.close();
	}

	private static void solve(String line) {
		String[] input = line.split(" ");
		double L = Double.parseDouble(input[0]);
		double W = Double.parseDouble(input[1]);
		double a = 12;
		double b = -4 * (W + L);
		double c = W * L;
		double D = b * b - 4 * a * c;
		double x1 = (-b + Math.sqrt(D)) / (2 * a);
		double x2 = (-b - Math.sqrt(D)) / (2 * a);

		System.out.printf("%.3f %.3f %.3f\n", Math.min(x1, x2), 0.0,
				Math.min(L / 2, W / 2));
	}
}
