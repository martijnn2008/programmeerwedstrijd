package urionlinejudge_com_br;

// 1039 - ACCEPTED

import java.util.*;

public class FireFlowers {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (sc.hasNextInt()) {
			solve(sc);
		}
		sc.close();
	}

	public static void solve(Scanner sc) {
		int r1 = sc.nextInt();
		int x1 = sc.nextInt();
		int y1 = sc.nextInt();
		int r2 = sc.nextInt();
		int x2 = sc.nextInt();
		int y2 = sc.nextInt();

		if (r2 > r1) {
			System.out.println("MORTO");
		} else if (r2 == r1) {
			if (x1 == x2 && y1 == y2) {
				System.out.println("RICO");
			} else {
				System.out.println("MORTO");
			}
		} else {
			int A = x1 - x2;
			int B = y1 - y2;
			double dist = Math.sqrt(A * A + B * B);
			if(dist + r2  <= r1) {
				System.out.println("RICO");
			} else {
				System.out.println("MORTO");
			}
		}
	}
}
