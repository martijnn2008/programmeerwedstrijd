package urionlinejudge_com_br;

// ACCEPTED - 1549

import java.io.*;

public class SplittingTheCoke {
	public static void main(String[] args) throws Exception {
		InputStreamReader ir = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(ir);

		int n = Integer.parseInt(in.readLine());
		for (int i = 0; i < n; i++) {
			String[] line1 = in.readLine().split(" ");
			String[] line2 = in.readLine().split(" ");
			double N = (double) Integer.parseInt(line1[0]);
			double L = (double) Integer.parseInt(line1[1]);
			double b = (double) Integer.parseInt(line2[0]);
			double B = (double) Integer.parseInt(line2[1]);
			double H = (double) Integer.parseInt(line2[2]);
			solve(N, L, b, B, H);
		}
		ir.close();
		in.close();
	}

	static void solve(double N, double L, double b, double B, double H) {
		if (b == B) {
			double h = L / N / Math.PI / B / B;
			System.out.printf("%.2f\n", h);
		} else {
			double x = b * H / (B - b);
			double I = L / N + Math.PI / 3 * b * b * x;
			double h = Math.pow(I / Math.PI * 3 / (b / x) / (b / x), 1.0 / 3.0);
			System.out.printf("%.2f\n", h - x);
		}
	}
}
