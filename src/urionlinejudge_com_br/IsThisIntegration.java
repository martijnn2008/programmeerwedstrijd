package urionlinejudge_com_br;

// ACCEPTED - UVa - 10209 
// WA - URI

import java.io.*;

public class IsThisIntegration {
	public static void main(String[] args) throws Exception {
		InputStreamReader ir = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(ir);

		String line;
		while ((line = in.readLine()) != null) {
			solve(line);
		}
		ir.close();
		in.close();
	}

	private static void solve(String line) {
		double r = Double.parseDouble(line);

		double C = r * r - Math.PI * r * r / 2 + Math.PI * r * r / 3 - r * r
				* Math.sqrt(3.0 / 4.0) / 2;
		double B = r * r - Math.PI * r * r / 4 - 2 * C;
		double A = r * r - 4 * B - 4 * C;

		System.out.printf("%.3f %.3f %.3f\n", A, 4 * B, 4 * C);
	}
}
