package urionlinejudge_com_br;

import java.io.*;

public class TobogganOfMarbles {

	public static void main(String[] args) throws Exception {
		InputStreamReader ir = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(ir);

		String line;
		while ((line = in.readLine()) != null) {
			int n = Integer.parseInt(line);
			solve(n, in);
		}

		ir.close();
		in.close();
	}

	private static void solve(int n, BufferedReader in) throws Exception {
		String[] line = in.readLine().split(" ");
		int L = Integer.parseInt(line[0]);
		int H = Integer.parseInt(line[1]);

		int[] y1 = new int[n];
		int[] x1 = new int[n];
		int[] y2 = new int[n];
		for (int i = 0; i < n; i++) {
			String[] input = in.readLine().split(" ");
			y1[i] = Integer.parseInt(input[0]);
			x1[i] = Integer.parseInt(input[1]);
			y2[i] = Integer.parseInt(input[2]);
		}

	}
}
