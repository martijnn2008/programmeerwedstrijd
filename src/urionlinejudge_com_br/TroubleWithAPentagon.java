package urionlinejudge_com_br;

// ACCEPTED - UVa - 10286
// WA - URI

import java.io.*;

public class TroubleWithAPentagon {

	public static void main(String[] args) throws Exception {
		InputStreamReader ir = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(ir);

		String line;
		while ((line = in.readLine()) != null) {
			solve(line);
		}
		ir.close();
		in.close();
	}

	private static void solve(String line) {
		double x = Double.parseDouble(line);
		double y = x * Math.sin(72*Math.PI/180)/Math.sin(63*Math.PI/180);
		System.out.printf("%.10f\n", y);
	}
}
