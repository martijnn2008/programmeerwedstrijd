package uva_onlinejudge_org;

// 492 Pig-Latin
// ACCEPTED

import java.io.*;

public class PigLatin {
    public static void main(String[] args) throws Exception {
        // BufferedReader is a lot quicker than Scanner
        DataInputStream in = new DataInputStream(System.in);
        DataOutputStream out = new DataOutputStream(System.out);
        int c;

        int temp = -1;
        while ((c = in.read()) != -1) {
            if (isLetter(c)) {
                if (!isVowel(c) && temp == -1) {
                    temp = c;
                } else {
                    out.write(c);
                }
                if (temp == -1)
                    temp = 0;
            } else {
                if (temp != -1) {
                    if (temp != 0)
                        out.write((char) temp);
                    out.write('a');
                    out.write('y');
                }
                if (c == 10) {
                    out.write('\n');
                } else {
                    out.write(c);
                }
                temp = -1;
            }
        }
    }

    public static boolean isLetter(int c) {
        return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z');
    }

    public static boolean isVowel(int c) {
        return c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U'
                || c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }
}
