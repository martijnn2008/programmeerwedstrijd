package uva_onlinejudge_org;

// 495 - Fibonacci Freeze
// ACCEPTED

import java.math.BigInteger;
import java.util.*;

public class FibonacciFreeze {
    static ArrayList<BigInteger> fibo;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        fibo = new ArrayList<BigInteger>();
        fibo.add(new BigInteger("0"));
        fibo.add(new BigInteger("1"));
        while (sc.hasNextInt()) {
            solve(sc.nextInt());
        }
        sc.close();
    }

    private static void solve(int n) {
        while (fibo.size() <= n) {
            int x = fibo.size();
            fibo.add(fibo.get(x - 1).add(fibo.get(x - 2)));
        }
        System.out.println("The Fibonacci number for " + n + " is " + fibo.get(n));
    }
}
