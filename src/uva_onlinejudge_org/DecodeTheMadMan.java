package uva_onlinejudge_org;

import java.io.*;

// 10222 - Decode The Mad Man
// ACCEPTED

public class DecodeTheMadMan {
    public static void main(String[] args) throws Exception {
        BufferedReader bi = new BufferedReader(new InputStreamReader(System.in));
        String line = bi.readLine();
        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            if (c == ' ') {
                System.out.print(' ');
            } else {
                System.out.print(replace(Character.toLowerCase(c)));
            }
        }
        System.out.println();
    }

    public static char replace(char c) {
        switch (c) {
            case 'e': return 'q'; case 'd': return 'a'; case 'c': return 'z';
            case 'r': return 'w'; case 'f': return 's'; case 'v': return 'x';
            case 't': return 'e'; case 'g': return 'd'; case 'b': return 'c';
            case 'y': return 'r'; case 'h': return 'f'; case 'n': return 'v';
            case 'u': return 't'; case 'j': return 'g'; case 'm': return 'b';
            case 'i': return 'y'; case 'k': return 'h'; case ',': return 'n';
            case 'o': return 'u'; case 'l': return 'j'; case '.': return 'm';
            case 'p': return 'i'; case ';': return 'k';
            case '[': return 'o'; case '\'':return 'l';
            case ']': return 'p';
        }
        return 0;
    }
}
