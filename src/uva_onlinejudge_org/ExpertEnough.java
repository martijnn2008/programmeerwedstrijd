package uva_onlinejudge_org;

// 1237 - Expert Enough?
// ACCEPTED

import java.util.*;

public class ExpertEnough {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        solve(sc);
        for (int i = 1; i < n; ++i) {
            System.out.println();
            solve(sc);
        }
    }

    private static void solve(Scanner sc) {
        int d = sc.nextInt();
        String[] names = new String[d];
        int[] low = new int[d];
        int[] high = new int[d];
        while (0 <= --d) {
            names[d] = sc.next();
            low[d] = sc.nextInt();
            high[d] = sc.nextInt();
        }
        int q = sc.nextInt();
        while (q-- > 0) {
            query(sc.nextInt(), names, low, high);
        }
    }

    private static void query(int query, String[] names, int[] low, int[] high) {
        int n = 0;
        String output = "";
        for (int i = 0; i < names.length; ++i) {
            if (low[i] <= query && query <= high[i]) {
                n++;
                output = names[i];
            }
        }
        if (n == 1) {
            System.out.println(output);
        } else {
            System.out.println("UNDETERMINED");
        }
    }
}
