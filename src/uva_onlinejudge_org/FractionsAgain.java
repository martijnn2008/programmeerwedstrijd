package uva_onlinejudge_org;

// 10976 - Fractions Again?!
// ACCEPTED

import java.util.*;

public class FractionsAgain {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            solve(sc.nextInt());
        }
        sc.close();
    }

    private static void solve(int k) {
        List<String> output = new ArrayList<String>();
        for (int x = k + 1; x <= k * 2; ++x) {
            if (x - k <= 0)
                continue;
            int g = gcd(x - k, x * k);
            if ((x - k) / g != 1)
                continue;
            int y = k * x / g;
            output.add("1/" + k + " = 1/" + y + " + 1/" + x);
        }
        System.out.println(output.size());
        for (String s : output) {
            System.out.println(s);
        }
    }

    private static int gcd(int a, int b) {
        while (b != 0 && a != 0) {
            if (a > b)
                a = a % b;
            else
                b = b % a;
        }
        return a > b ? a : b;
    }
}
