package uva_onlinejudge_org;

// 10696 - f91
// ACCEPTED

import java.io.*;

public class f91 {
    public static void main(String[] args) throws Exception {
        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = sc.readLine()) != null) {
            int n = Integer.parseInt(line);
            if (n == 0)
                break;
            solve(n);
        }
        sc.close();
    }

    private static void solve(int n) {
        System.out.println("f91(" + n + ") = " + f(n));
    }

    private static int f(int n) {
        if (n <= 101)
            return 91;
        else
            return n - 10;
    }
}
