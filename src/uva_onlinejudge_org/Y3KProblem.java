package uva_onlinejudge_org;

// 893 - Y3K Problem
// ACCEPTED

import java.util.Scanner;

public class Y3KProblem {
    public static class Date {
        int day, month, year;
        int[] month_days = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        public Date(int DD, int MM, int YY) {
            day = DD;
            month = MM;
            year = YY;
        }

        public void add(int days) {
            day--;
            month--;
            while (days >= monthDays(month) - day) {
                days -= monthDays(month) - day;
                day = 0;
                month++;
                if (month == 12) {
                    month = 0;
                    year++;
                }
            }
            day += days;
            day++;
            month++;
        }

        public int monthDays(int month) {
            return month_days[month] + (isLeap() && month == 1 ? 1 : 0);
        }

        public boolean isLeap() {
            if (year % 4 != 0)
                return false;
            if (year % 100 != 0)
                return true;
            if (year % 400 != 0)
                return false;
            return true;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int d = sc.nextInt();
            int DD = sc.nextInt();
            int MM = sc.nextInt();
            int YY = sc.nextInt();

            if (d == 0 && DD == 0 && MM == 0 && YY == 0) {
                break;
            }

            solve(d, DD, MM, YY);
        }
        sc.close();
    }

    static void solve(int d, int DD, int MM, int YY) {
        Date date = new Date(DD, MM, YY);
        date.add(d);
        System.out.println(date.day + " " + date.month + " " + date.year);
    }
}
