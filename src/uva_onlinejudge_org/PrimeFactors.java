package uva_onlinejudge_org;

// CORRECT
// 583 - Prime Factors

import java.util.*;

public class PrimeFactors {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            int n = sc.nextInt();
            if (n == 0) break;
            solve(n);
        }
    }

    public static void solve(int n) {
        System.out.print(n + " = ");
        if (n < 0) {
            n *= -1;
            System.out.print("-1 x ");
        }
        List<Integer> factors = primeFactors(n);
        System.out.print(factors.get(0));
        for (int i = 1; i < factors.size(); ++i) {
            System.out.print(" x " + factors.get(i));
        }
        System.out.println();
    }

    public static List<Integer> primeFactors(int numbers) {
        int n = numbers;
        List<Integer> factors = new ArrayList<Integer>();
        for (int i = 2; i <= n / i; ++i) {
            while (n % i == 0) {
                factors.add(i);
                n /= i;
            }
        }
        if (n > 1) {
            factors.add(n);
        }
        return factors;
    }
}
