package uva_onlinejudge_org;


// 10474 - Where is the Marble?

import java.util.*;

public class WhereIsTheMarble {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int i = 1;
        while (true) {
            int n = sc.nextInt();
            int q = sc.nextInt();
            if(n == 0 && q == 0) break;
            System.out.println("CASE# " + (i++) + ":");
            solve(n , q, sc);
        }
    }

    public static void solve(int n, int q, Scanner sc) {
        int[] input = new int[n];
        for (int i = 0; i < n; i++) {
            input[i] = sc.nextInt();
        }
        Arrays.sort(input);
        for (int i = 0; i < q; i++) {
            int x = sc.nextInt();
            int index = binary_search(x, input);
            if (index == -1) {
                System.out.println(x + " not found");
            } else {
                System.out.println(x + " found at " + (index + 1));
            }
        }
    }

    public static int binary_search(int x, int[] array) {
        int low = 0;
        int high = array.length - 1;
        while (low < high) {
            int mid = (low + high) / 2;
            if (x > array[mid]) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }
        if(array[low] == x) return low;
        return -1;
    }
}
