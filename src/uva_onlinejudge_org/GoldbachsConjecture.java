package uva_onlinejudge_org;

// 543 - Goldbach's Conjecture
// ACCEPTED

import java.util.*;

public class GoldbachsConjecture {
    static boolean primes[] = new boolean[1000001];

    public static void main(String[] args) {
        calculatePrimes();

        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            if (n == 0)
                break;
            solve(n);
        }
        sc.close();
    }

    public static void solve(int n) {
        for(int i = 2; i <= n / 2; i++) {
            if(primes[i] && primes[n-i]) {
                System.out.println(n + " = " + i + " + " + (n-i));
                return;
            }
        }
        System.out.println("Goldbach's conjecture is wrong.");
    }

    public static void calculatePrimes() {
        Arrays.fill(primes, true);
        primes[0] = false;
        primes[1] = false;

        for (int i = 2; i < 1000001; i++) {
            if (primes[i]) {
                for (int k = 2 * i; k < 1000001; k += i) {
                    primes[k] = false;
                }
            }
        }
    }
}
