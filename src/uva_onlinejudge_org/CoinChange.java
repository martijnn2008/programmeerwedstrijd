package uva_onlinejudge_org;

import java.util.*;

// CORRECT
// 674 - Coin Change

public class CoinChange {
    static long[][] table = new long[7489 + 1][5];

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] S = new int[]{1, 5, 10, 25, 50};
        count(S, 5, 7489);
        while (sc.hasNextInt()) {
            int n = sc.nextInt();
            System.out.println(table[n][4]);
        }
    }

    public static long count(int S[], int m, int n) {


        for (int i = 0; i < m; i++) {
            table[0][i] = 1;
        }
        for (int i = 1; i < n + 1; i++) {
            for (int j = 0; j < m; j++) {
                long x = (i - S[j] >= 0) ? table[i - S[j]][j] : 0;
                long y = (j >= 1) ? table[i][j - 1] : 0;
                table[i][j] = x + y;
            }
        }
        return table[n][m - 1];
    }
}
