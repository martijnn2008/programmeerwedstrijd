package uva_onlinejudge_org;

// CORRECT
// 11631 - Dark roads

import java.util.*;

public class DarkRoads {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        while(n != 0 && m != 0) {
            solve(n, m, sc);
            n = sc.nextInt();
            m = sc.nextInt();
        }
    }

    static class Edge implements Comparable<Edge> {
        int a, b, w;
        public Edge(int a, int b, int w) {
            this.a = a;
            this.b = b;
            this.w = w;
        }

        public int compareTo(Edge e) {
            return this.w - e.w;
        }

        public String toString() {
            return "[(" + a + ", " + b + ") : " + w + "]";
        }
    }

    static void solve(int n, int m, Scanner sc) {
        Edge[] edges = new Edge[m];
        Edge[] tree = new Edge[n - 1];
        int total = 0;
        for (int i = 0; i < m; i++) {
            edges[i] = new Edge(sc.nextInt(), sc.nextInt(), sc.nextInt());
            total += edges[i].w;
        }
        parent = new int[n];
        rank = new int[n];
        int s = 0;
        Arrays.sort(edges);
        for (int i = 0; i < n; i++) { parent[i] = i; }
        for (Edge e : edges) { if (join(e.a, e.b)) { tree[s++] = e; }}
        int output = 0;
        for (Edge e : tree) {
            output += e.w;
        }
        System.out.println(total - output);
    }

    static int[] parent;
    static int[] rank;
    static boolean join(int x, int y) {
        int xrt = find(x);
        int yrt = find(y);
        if (rank[xrt] > rank[yrt]) {
            parent[yrt] = xrt;
        } else if (rank[xrt] < rank[yrt]) {
            parent[xrt] = yrt;
        } else if (xrt != yrt) {
            rank[parent[yrt]=xrt]++;
        }
        return xrt != yrt;
    }

    static int find(int x) {
        return parent[x] == x ? x : (parent[x] = find(parent[x]));
    }

}