package uva_onlinejudge_org;

// 10718 - Bit Mask
// ACCEPTED

import java.util.*;

public class BitMask {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLong()) {
            solve(sc.nextLong(), sc.nextLong(), sc.nextLong());
        }
        sc.close();
    }

    private static void solve(long N, long L, long U) {
        long M = 0;
        for (int i = 31; i >= 0; --i) {
            if ((N & 1L << i) == 0) {
                if (M + (1L << i) <= U) {
                    M += (1L << i);
                }
            } else {
                if (M + (1L << i) - 1 < L) {
                    M += (1L << i);
                }
            }
        }
        System.out.println(M);
    }
}
