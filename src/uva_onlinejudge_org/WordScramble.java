package uva_onlinejudge_org;

// 483 - Word Scramble 
// ACCEPTED

import java.io.*;

public class WordScramble {

    public static void main(String[] args) throws Exception {
        // BufferedReader is a lot quicker than Scanner
        BufferedReader bi = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = bi.readLine()) != null) {
            solve(line);
        }
    }

    public static void solve(String input) {
        String words[] = input.split(" ");
        reverse(words[0]);
        for (int i = 1; i < words.length; i++) {
            System.out.print(" ");
            reverse(words[i]);
        }
        System.out.println();
    }

    public static void reverse(String word) {
        for (int i = word.length() - 1; i >= 0; i--) {
            System.out.print(word.charAt(i));
        }
    }
}
