package uva_onlinejudge_org;

import java.math.BigInteger;
import java.util.*;

// 10925 - Krakovia
// ACCEPTED

public class Krakovia {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int i = 1;
        while (sc.hasNextInt()) {
            int n = sc.nextInt();
            int f = sc.nextInt();
            if (n == 0 && f == 0)
                break;
            System.out.print("Bill #" + i + " costs ");
            i++;
            solve(n, f, sc);
            System.out.println();
        }
        sc.close();
    }

    public static void solve(int n, int f, Scanner sc) {
        BigInteger total = new BigInteger("0");
        for(int i = 0; i < n; i++) {
            total = total.add(new BigInteger(sc.next()));
        }
        System.out.print(total);
        System.out.print(": each friend should pay ");
        System.out.println(total.divide(new BigInteger(Integer.toString(f))));
    }
}
