package uva_onlinejudge_org;

// 481 - What Goes Up
// ACCEPTED

import java.util.*;

public class WhatGoesUp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> input = new ArrayList<Integer>();
        while (sc.hasNextInt()) {
            input.add(sc.nextInt());
        }
        solve(input);
        sc.close();
    }

    private static void solve(List<Integer> input) {
        ArrayList<Integer> maxsize = new ArrayList<Integer>();
        int[] parent = new int[input.size()];
        Arrays.fill(parent, -1);
        int size = 0;
        maxsize.add(0);

        for (int i = 1; i < input.size(); ++i) {
            int c = input.get(i);
            if (c > input.get(maxsize.get(size))) {
                parent[i] = maxsize.get(size);
                maxsize.add(i);
                ++size;
            } else {
                int place = binairySearch(input, maxsize, c);
                if (place != 0)
                    parent[i] = maxsize.get(place - 1);
                maxsize.set(place, i);
            }
        }
        System.out.println(size + 1);
        System.out.println("-");

        int i = maxsize.get(size);
        ArrayList<Integer> output = new ArrayList<Integer>();
        while (i != -1) {
            output.add(input.get(i));
            i = parent[i];
        }

        for (int k = output.size() - 1; k >= 0; --k) {
            System.out.println(output.get(k));
        }
    }

    private static int binairySearch(List<Integer> input, List<Integer> list, int c) {
        int low = 0;
        int high = list.size() - 1;
        while (high > low) {
            int mid = (high + low) / 2;
            if (input.get(list.get(mid)) < c) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }
        return high;
    }
}
