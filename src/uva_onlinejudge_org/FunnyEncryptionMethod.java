package uva_onlinejudge_org;

import java.util.Scanner;

// 10019 - Funny Encryption Method
// ACCEPTED

public class FunnyEncryptionMethod {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            solve(sc.nextInt());
        }
        sc.close();
    }

    public static void solve(int n) {
        int b1 = Integer.bitCount(n);
        int x2 = Integer.parseInt(Integer.toString(n), 16);
        int b2 = Integer.bitCount(x2);
        System.out.println(b1 + " " + b2);
    }
}
