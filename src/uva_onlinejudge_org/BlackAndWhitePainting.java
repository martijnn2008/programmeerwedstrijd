package uva_onlinejudge_org;

// 11231 - Black and white painting
// ACCEPTED

import java.util.*;

public class BlackAndWhitePainting {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            int c = sc.nextInt();
            if (n == 0 && m == 0 && c == 0)
                break;
            solve(n, m, c);
        }
        sc.close();
    }

    public static void solve(int n, int m, int c) {
        int output = (n - 7) * (m - 7) / 2;
        if (n % 2 == 0 && m % 2 == 0)
            output += c;
        System.out.println(output);
    }
}
