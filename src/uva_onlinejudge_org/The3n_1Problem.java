package uva_onlinejudge_org;

// 100 - The 3n + 1 problem
// ACCEPTED

import java.util.*;

public class The3n_1Problem {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            solve(sc.nextInt(), sc.nextInt());
        }
        sc.close();
    }

    public static void solve(int a, int b) {
        int i = Math.min(a, b);
        int j = Math.max(a, b);
        int output = 1;
        for (int k = i; k <= j; k++) {
            int temp = 1;
            int n = k;
            while (n != 1) {
                if (n % 2 != 0)
                    n = 3 * n + 1;
                else
                    n /= 2;
                temp++;
            }
            output = Math.max(output, temp);
        }
        System.out.println(a + " " + b + " " + output);
    }
}
