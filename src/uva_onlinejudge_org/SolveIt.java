package uva_onlinejudge_org;

// 10341 - Solve It
// TODO: Wrong Answer

import java.util.*;

public class SolveIt {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            solve(sc);
        }
        sc.close();
    }

    static int p, q, r, s, t, u;

    public static void solve(Scanner sc) {
        p = sc.nextInt();
        q = sc.nextInt();
        r = sc.nextInt();
        s = sc.nextInt();
        t = sc.nextInt();
        u = sc.nextInt();

        if (f(0) * f(1) > 0) {
            System.out.printf("No solution\n");
        } else {
            System.out.printf("%.4f\n", binarySearch());
        }
    }

    public static double binarySearch() {
        double low = 0.0;
        double high = 1.0;
        double mid = 0.5;
        while (Math.abs(high - low) > 0.00000001) {
            mid = (low + high) / 2;
            if (f(low) * f(mid) <= 0) {
                high = mid;
            } else {
                low = mid;
            }
        }
        return mid;
    }

    public static double f(double x) {
        return p * Math.pow(Math.E, -x) + q * Math.sin(x) + r * Math.cos(x) + s * Math.tan(x) + t * x * x + u;
    }
}
