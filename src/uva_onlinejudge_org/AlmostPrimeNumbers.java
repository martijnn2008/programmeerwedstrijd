package uva_onlinejudge_org;

// 10539 - Almost Prime Numbers
// TODO: Wrong Answer

import java.util.*;

public class AlmostPrimeNumbers {
    static boolean sieve[] = new boolean[1000001];
    static int primes[] = new int[78498]; // the ammount of primes in [1,1000000]

    public static void main(String[] args) {
        calculatePrimes();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            solve(sc.nextLong(), sc.nextLong());
        }
        sc.close();
    }

    public static void solve(long a, long b) {
        int output = 0;
        for (int prime : primes) {
            long temp = prime * prime;
            while(temp <= b) {
                if(temp >= a) {
                    output++;
                }
                temp *= prime;
            }
        }
        System.out.println(output);
    }

    public static void calculatePrimes() {
        Arrays.fill(sieve, true);
        sieve[0] = sieve[1] = false;

        for (int i = 2; i < 1000001; i++) {
            if (sieve[i]) {
                for (int k = 2 * i; k < 1000001; k += i) {
                    sieve[k] = false;
                }
            }
        }

        int n = 0;
        for (int i = 2; i < 1000001; i++) {
            if (sieve[i]) {
                primes[n] = i;
                n++;
            }
        }
    }
}
