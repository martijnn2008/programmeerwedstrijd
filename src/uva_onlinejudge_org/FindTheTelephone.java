package uva_onlinejudge_org;

// 10921 - Find the Telephone
// ACCEPTED

import java.util.*;

public class FindTheTelephone {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            solve(sc.nextLine());
        }
        sc.close();
    }

    public static void solve(String input) {
        for (int i = 0; i < input.length(); i++) {
            System.out.print(replace(input.charAt(i)));
        }
        System.out.println();
    }

    public static char replace(char c) {
        switch(c) {
                                          case '-': return '-';
                                          case '0': return '0';
                                          case '1': return '1';
                      case 'A': case 'B': case 'C': return '2';
                      case 'D': case 'E': case 'F': return '3';
                      case 'G': case 'H': case 'I': return '4';
                      case 'J': case 'K': case 'L': return '5';
                      case 'M': case 'N': case 'O': return '6';
            case 'P': case 'Q': case 'R': case 'S': return '7';
                      case 'T': case 'U': case 'V': return '8';
            case 'W': case 'X': case 'Y': case 'Z': return '9';
        }
        return 0;
    }
}
