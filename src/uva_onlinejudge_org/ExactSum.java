package uva_onlinejudge_org;

// 11057 - Exact Sum
// ACCEPTED

import java.util.*;

public class ExactSum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            solve(sc);
            System.out.println();
        }
        sc.close();
    }

    private static void solve(Scanner sc) {
        int n = sc.nextInt();
        int[] list = new int[n];
        while (n-- > 0) {
            list[n] = sc.nextInt();
        }
        int goal = sc.nextInt();

        Arrays.sort(list);
        int a = 0, b = Integer.MAX_VALUE / 3;
        for (int i : list) {
            if (binairySearch(goal - i, list) && Math.abs(goal - i - i) < (b - a)) {
                a = Math.min(goal - i, i);
                b = Math.max(goal - i, i);
            }
        }

        System.out.println("Peter should buy books whose prices are " + a + " and " + b + ".");
    }

    private static boolean binairySearch(int i, int[] list) {
        int low = 0;
        int high = list.length - 1;
        while (low < high) {
            int mid = (low + high) / 2;
            if (i > list[mid]) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }
        return list[low] == i;
    }
}
