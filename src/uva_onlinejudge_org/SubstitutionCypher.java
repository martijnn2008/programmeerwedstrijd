package uva_onlinejudge_org;

// 865 - Substitution Cypher
// ACCEPTED

import java.util.*;

public class SubstitutionCypher {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.nextLine();
        sc.nextLine();
        solve(sc);
        for (int i = 1; i < n; i++) {
            System.out.println();
            solve(sc);
        }
        sc.close();
    }

    public static void solve(Scanner sc) {
        String plaintext = sc.nextLine();
        String substitut = sc.nextLine();
        System.out.println(substitut);
        System.out.println(plaintext);

        Map<Character, Character> map = new HashMap<Character, Character>();
        for (int i = 0; i < plaintext.length(); i++) {
            map.put(plaintext.charAt(i), substitut.charAt(i));
        }

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.equals(""))
                break;
            for (int i = 0; i < line.length(); i++) {
                char c = line.charAt(i);
                if (map.containsKey(c)) {
                    System.out.print(map.get(c));
                } else {
                    System.out.print(c);
                }
            }
            System.out.println();
        }
    }
}
