package uva_onlinejudge_org;

// 10567 - Helping Fill Bates
// ACCEPTED

import java.util.*;
import java.io.*;

public class HelpingFillBates {
    public static void main(String[] args) throws Exception {
        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        String inputS = sc.readLine();
        Map<Character, ArrayList<Integer>> input = preprocess(inputS);
        int q = Integer.parseInt(sc.readLine());
        for (int i = 0; i < q; i++) {
            solve(input, sc.readLine());
        }
        sc.close();

    }

    private static Map<Character, ArrayList<Integer>> preprocess(String inputS) {
        Map<Character, ArrayList<Integer>> output = new HashMap<Character, ArrayList<Integer>>();
        for (int i = 0; i < inputS.length(); ++i) {
            char c = inputS.charAt(i);
            ArrayList<Integer> temp = output.get(c);
            if (temp == null) {
                temp = new ArrayList<Integer>();
            }
            temp.add(i);
            output.put(c, temp);
        }
        return output;
    }

    private static void solve(Map<Character, ArrayList<Integer>> input, String query) {
        int low = -1;
        int high = -1;
        boolean solution = true;
        for (int i = 0; i < query.length(); ++i) {
            ArrayList<Integer> list = input.get(query.charAt(i));
            if (list == null) {
                solution = false;
                break;
            }
            int temp = binairySearch(high + 1, list);
            if (temp == -1) {
                solution = false;
                break;
            }
            if (low == -1) {
                low = temp;
                high = temp;
            } else {
                high = temp;
            }
        }
        if (solution) {
            System.out.println("Matched " + low + " " + high);
        } else {
            System.out.println("Not matched");
        }
    }

    private static int binairySearch(int n, ArrayList<Integer> list) {
        int low = 0;
        int high = list.size() - 1;
        while (low < high) {
            int mid = (low + high) / 2;
            if (n > list.get(mid)) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }
        if (list.get(low) >= n) {
            return list.get(low);
        }
        return -1;
    }
}
