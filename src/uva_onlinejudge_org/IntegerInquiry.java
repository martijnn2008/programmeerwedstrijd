package uva_onlinejudge_org;

// 424 - Integer Inquiry
// ACCEPTED

import java.io.*;
import java.math.BigInteger;

public class IntegerInquiry {
    public static void main(String[] args) throws Exception {
        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        BigInteger output = BigInteger.ZERO;
        String line;
        while ((line = sc.readLine()) != null) {
            BigInteger temp = new BigInteger(line);
            output = output.add(temp);
        }
        System.out.println(output);
        sc.close();
    }
}
