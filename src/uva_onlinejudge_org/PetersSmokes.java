package uva_onlinejudge_org;

// 10346 - Peter's Smokes
// ACCEPTED

import java.util.*;

public class PetersSmokes {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            solve(sc.nextInt(), sc.nextInt());
        }
        sc.close();
    }

    public static void solve(int n, int k) {
        int output = n;
        int remainder = n;
        while (remainder / k > 0) {
            output += remainder / k;
            remainder = remainder / k + remainder % k;
        }
        System.out.println(output);
    }
}
