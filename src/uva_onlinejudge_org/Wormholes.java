package uva_onlinejudge_org;

// 558 - Wormholes
// CORRECT
import java.util.*;

public class Wormholes {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            solve(sc);
        }
    }

    static class Edge {
        public int s, d, w;

        public Edge(int s, int d, int w) {
            this.s = s;
            this.d = d;
            this.w = w;
        }
    }

    public static void solve(Scanner sc) {
        int n = sc.nextInt();
        int m = sc.nextInt();
        Edge[] edges = new Edge[m];
        for (int i = 0; i < m; i++) {
            edges[i] = new Edge(sc.nextInt(), sc.nextInt(), sc.nextInt());
        }

        int[] d = new int[n];
        Arrays.fill(d, Integer.MAX_VALUE);
        d[0] = 0;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < m; j++) {
                Edge e = edges[j];
                if (d[e.d] > d[e.s] + e.w)
                    d[e.d] = d[e.s] + e.w;
            }
        }
        boolean flag = false;
        // reporting
        for (int j = 0; j < m; j++) {
            Edge e = edges[j];
            if (d[e.d] > d[e.s] + e.w) {
                flag = true;
                break;
            }
        }
        if(flag) {
            System.out.println("possible");
        } else {
            System.out.println("not possible");
        }

    }
}
