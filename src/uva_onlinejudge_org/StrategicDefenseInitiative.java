package uva_onlinejudge_org;

// 497 - Strategic Defense Initiative
// ACCEPTED

import java.util.*;

public class StrategicDefenseInitiative {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int aantal = Integer.valueOf(sc.nextLine());
        sc.nextLine();
        solve(sc);
        for (int i = 1; i < aantal; ++i) {
            System.out.println();
            solve(sc);
        }
        sc.close();
    }

    private static void solve(Scanner sc) {
        List<Integer> input = new ArrayList<Integer>();
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.equals(""))
                break;
            input.add(Integer.valueOf(line));
        }

        solve(input);
    }

    private static void solve(List<Integer> input) {
        List<Integer> maxsize = new ArrayList<Integer>();
        int[] parent = new int[input.size()];
        Arrays.fill(parent, -1);
        maxsize.add(0);
        int size = 0;

        for (int i = 1; i < input.size(); i++) {
            if (input.get(i) > input.get(maxsize.get(size))) {
                parent[i] = maxsize.get(size);
                maxsize.add(i);
                ++size;
            } else {
                int place = binairySearch(input, maxsize, i);
                if (place != 0)
                    parent[i] = maxsize.get(place - 1);
                maxsize.set(place, i);
            }
        }

        int i = maxsize.get(size);
        List<Integer> output = new ArrayList<Integer>();
        while (i != -1) {
            output.add(input.get(i));
            i = parent[i];
        }

        System.out.println("Max hits: " + (size + 1));
        for (int k = output.size() - 1; k >= 0; --k) {
            System.out.println(output.get(k));
        }
    }

    private static int binairySearch(List<Integer> input, List<Integer> maxsize, int i) {
        int low = 0;
        int high = maxsize.size() - 1;
        while (low < high) {
            int mid = (low + high) / 2;
            if (input.get(maxsize.get(mid)) < input.get(i)) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }
        return low;
    }

}
