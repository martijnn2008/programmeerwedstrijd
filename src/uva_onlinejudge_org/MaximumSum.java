package uva_onlinejudge_org;

// 108 - Maximum Sum
// ACCEPTED

import java.util.*;

public class MaximumSum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int[][] input = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                input[i][k] = sc.nextInt();
            }
        }

        sc.close();

        int[][] pp = new int[n + 1][n + 1];
        for (int i = 1; i <= n; i++) {
            for (int k = 1; k <= n; k++) {
                int a = 0, b = 0, c = 0;
                if (i - 1 >= 0)
                    a = pp[i - 1][k];
                if (k - 1 >= 0)
                    b = pp[i][k - 1];
                if (i - 1 >= 0 && k - 1 >= 0)
                    c = pp[i - 1][k - 1];
                pp[i][k] = a + b - c + input[i - 1][k - 1];
            }
        }

        int output = Integer.MIN_VALUE;
        for (int i = 1; i <= n; i++) {
            for (int k = 1; k <= n; k++) {
                for (int a = 0; a < i; a++) {
                    for (int b = 0; b < k; b++) {
                        int temp = pp[i][k] + pp[a][b] - pp[a][k] - pp[i][b];
                        output = Math.max(output, temp);
                    }
                }
            }
        }
        System.out.println(output);
    }
}