package uva_onlinejudge_org;

// ACCEPTED
// 10298 - Power Strings

import java.util.*;
public class PowerStrings {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String input = sc.next();
            if (input.equals(".")) break;
            solve(input);
        }
    }

    private static void solve(String input) {
        int n = input.length();
        for (int i = n; i > 0; i--) {
            if (n % i != 0) continue;
            boolean good = true;
            for (int k = 0; (k + 2) * (n / i) <= n; k++) {
                if (!input.substring(k * (n / i), (k + 1) * (n / i)).equals(input.substring((k + 1) * (n / i), (k + 2) * (n / i)))) {
                    good = false;
                }
            }
            if (good) {
                System.out.println(i);
                return;
            }
        }
    }
}