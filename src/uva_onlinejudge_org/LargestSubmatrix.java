package uva_onlinejudge_org;

// 836 - Largest Submatrix
// ACCEPTED

import java.util.*;

public class LargestSubmatrix {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.valueOf(sc.nextLine()) - 1;
        sc.nextLine();
        solve(readInput(sc));
        while (n-- > 0) {
            sc.nextLine();
            System.out.println();
            solve(readInput(sc));
        }
        sc.close();
    }

    private static void solve(int[][] matrix) {
        int N = matrix.length;
        int[][] sum = new int[N + 1][N + 1];

        for (int i = 0; i < N; ++i) {
            for (int k = 0; k < N; ++k) {
                sum[i + 1][k + 1] = sum[i][k + 1] + sum[i + 1][k] - sum[i][k] + matrix[i][k];
            }
        }

        int output = 0;
        for (int x = 0; x <= N; ++x) {
            for (int y = 0; y <= N; ++y) {
                for (int a = 0; a < x; ++a) {
                    for (int b = 0; b < y; ++b) {
                        int c = sum[x][y] - sum[a][y] - sum[x][b] + sum[a][b];
                        output = Math.max(output, c);
                    }
                }
            }
        }
        System.out.println(output);
    }

    private static int[][] readInput(Scanner sc) {
        String line = sc.nextLine();
        int[][] matrix = new int[line.length()][line.length()];

        for (int k = 0; k < line.length(); ++k) {
            if (line.charAt(k) == '0') {
                matrix[0][k] = -1000;
            } else {
                matrix[0][k] = 1;
            }
        }

        for (int i = 1; i < line.length(); ++i) {
            line = sc.nextLine();
            for (int k = 0; k < line.length(); ++k) {
                if (line.charAt(k) == '0') {
                    matrix[i][k] = -1000;
                } else {
                    matrix[i][k] = 1;
                }
            }
        }
        return matrix;
    }
}
