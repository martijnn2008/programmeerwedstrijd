package uva_onlinejudge_org;

// ACCEPTED
// 902 - Password Search

import java.util.*;
public class PasswordSearch {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            solve(sc.nextInt(), sc.next());
        }
    }

    public static void solve(int N, String phrase) {
        HashMap<String, Integer> passwords = new HashMap<String, Integer>();
        for (int i = 0; i < phrase.length() - N; i++) {
            String password = phrase.substring(i, i + N);
            if (passwords.containsKey(password)) {
                passwords.put(password, passwords.get(password) + 1);
            } else {
                passwords.put(password, 1);
            }
        }

        String pass = "";
        int best = 0;
        for (String temp : passwords.keySet()) {
            if (passwords.get(temp) > best) {
                best = passwords.get(temp);
                pass = temp;
            }
        }
        System.out.println(pass);
    }
}