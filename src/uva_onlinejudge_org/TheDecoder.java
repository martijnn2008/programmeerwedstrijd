package uva_onlinejudge_org;

// 458 - The Decoder
// ACCEPTED

import java.io.*;

public class TheDecoder {
    public static void main(String[] args) throws Exception {
        DataInputStream in = new DataInputStream(System.in);
        DataOutputStream output = new DataOutputStream(System.out);
        int c;
        while ((c = in.read()) != -1) {
            if (c == 10) {
                output.write('\n');
            } else {
                output.write(c - 7);
            }
        }
    }
}