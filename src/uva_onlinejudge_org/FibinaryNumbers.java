package uva_onlinejudge_org;

// 763 - Fibinary Numbers
// ACCEPTED

import java.io.*;
import java.math.BigInteger;

public class FibinaryNumbers {
    static BigInteger[] fibbo = new BigInteger[120];

    public static void main(String[] args) throws Exception {
        preprocess();

        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        String line1;
        String line2;
        while ((line1 = sc.readLine()) != null && (line2 = sc.readLine()) != null) {
            solve(line1, line2);
            if (sc.readLine() == null)
                break;
            else
                System.out.println();
        }
        sc.close();
    }

    private static void solve(String line1, String line2) {
        BigInteger a = fromFibinary(line1);
        BigInteger b = fromFibinary(line2);
        System.out.println(toFibinary(a.add(b)));
    }

    private static String toFibinary(BigInteger n) {
        if (n.equals(BigInteger.ZERO)) {
            return "0";
        }

        StringBuilder output = new StringBuilder(100);
        boolean one = false;
        for (int i = 119; i >= 0; --i) {
            if (n.compareTo(fibbo[i]) != -1) {
                n = n.subtract(fibbo[i]);
                if (i > 1) {
                    output.append(10);
                    --i;
                    one = true;
                } else {
                    output.append(1);
                }
            } else if (one) {
                output.append(0);
            }
        }
        return output.toString();
    }

    private static BigInteger fromFibinary(String input) {
        BigInteger output = new BigInteger("0");
        int k = 0;
        for (int i = input.length() - 1; i >= 0; --i) {
            if (input.charAt(i) == '1')
                output = output.add(fibbo[k]);
            k++;
        }
        return output;
    }

    private static void preprocess() {
        fibbo[0] = new BigInteger("1");
        fibbo[1] = new BigInteger("2");
        for (int i = 2; i < 120; i++) {
            fibbo[i] = fibbo[i - 1].add(fibbo[i - 2]);
        }
    }
}
