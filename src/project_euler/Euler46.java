package project_euler;

public class Euler46 {
    public static void main(String[] args) {
        int i = 3;
        while (isPrime(i) || trueConjecture(i)) {
            i += 2;
        }
        System.out.println(i);
    }

    public static boolean isPrime(int n) {
        if (n == 1) return false;
        if (n == 2 || n == 3) return true;
        for (int i = 2; i * i <= n; i++) {
            if (n % i == 0) return false;
        }
        return true;
    }

    public static boolean trueConjecture(int n) {
        for (int i = 1; 2 * i * i < n; i++) {
            if (isPrime(n - 2 * i * i)) return true;
        }  
        return false;
    }
}
