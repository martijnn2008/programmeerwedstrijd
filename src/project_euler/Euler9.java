package project_euler;

import java.util.*;

public class Euler9 {
    public static void main(String[] args) {
        // find a + b + c = 1000
        // with a^2 + b^2 = c^2
        for (int a = 1; a < 1000; a++) {
            for (int b = 1; b + a < 1000; b++) {
                int c = (int) Math.sqrt(a * a + b * b);
                if (c * c != b * b + a * a) {
                    continue;
                }
                if (a + b + c == 1000) {
                    System.out.println(a * b * c);
                    return;
                }
            }
        }
    }
}
