package project_euler;

import java.util.*;

public class Euler4 {
    public static void main(String[] args) {
        int max = Integer.MIN_VALUE;
        for (int i = 999; i >= 100; i--) {
            for (int j = 999; j >= 100; j--) {
                if (isPalindrome(i * j)) {
                    max = Math.max(max, i * j);
                }
            }
        }
        System.out.println(max);
    }

    public static boolean isPalindrome(int n) {
        String temp = Integer.toString(n);
        String reverse = new StringBuilder(temp).reverse().toString();
        return reverse.equals(temp);
    }
}
