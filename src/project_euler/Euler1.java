package project_euler;

public class Euler1 {
    public static void main(String[] args) {
        int sum = 0;
        for(int i = 0, j = 0; i < 1000; i+=3, j+=5) {
            if (i % 5 != 0) {
                sum += i;
            }
            if (j < 1000) {
                sum += j;
            }
        }
        System.out.println(sum);
    }
}
