package project_euler;

public class Euler7 {
    public static void main(String[] args) {
        // 2, 3, 5, 7, 11, 13, 17
        int primeCounter = 1;
        int number = 1;
        while (primeCounter <= 10_000) {
            number += 2;
            if (isPrime(number)) {
                primeCounter++;
            }
        }
        System.out.println(number);
    }

    public static boolean isPrime(int n) {
        if (n % 2 == 0) {
            return false;
        }
        for(int div = 3; div * div <= n; div+=2) {
            if (n % div == 0) {
                return false;
            }
        }
        return true;
    }
}
