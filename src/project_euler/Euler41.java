package project_euler;

public class Euler41 {
    static int bestSolution;
    public static void main(String[] args) {
        String input = "987654321";
        for (int i = 0; i < 9; i++) {
            if(algorithm(input.substring(i, 9))) {
                System.out.println(bestSolution);
                return;
            }
        }
    }

    public static boolean algorithm(String input) {
        bestSolution = -1;
        algorithm("", input);
        return bestSolution != -1;
    }

    public static void algorithm(String prefix, String suffix) {
        int n = suffix.length();
        if (n == 0) {
            int number = Integer.parseInt(prefix);
            if (isPrime(number)) {
                bestSolution = Math.max(bestSolution, number);
            }
        } else {
            for (int i = 0; i < n; i++) {
                algorithm(prefix + suffix.charAt(i), suffix.substring(0, i) + 
                        suffix.substring(i + 1, n));
            }
        }
    }

    public static boolean isPrime(int n) {
        if (n < 2 || n % 2 == 0) return false;
        if (n == 2 || n == 3) return true;
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) return false;
        }
        return true;
    }
}
