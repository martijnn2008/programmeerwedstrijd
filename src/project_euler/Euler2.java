package project_euler;

public class Euler2 {
   public static void main(String[] args) {
        int a = 1;
        int b = 2;
        int sum = 0;
        while (a < 4_000_000) {
            int temp = a;
            a = b;
            b = temp + a;
            if (a % 2 == 0) {
                sum += a;
            }
        }
        System.out.println(sum);
   } 
}
