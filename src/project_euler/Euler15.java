package project_euler;

import java.util.*;

public class Euler15 {
    static long[][] mem = new long[21][21];
    public static void main(String[] args) {
        mem[0][0] = 1;
        for (int i = 0; i < 21; i++) {
            for (int j = 0; j < 21; j++) {
                if (j - 1 >= 0) {
                    mem[i][j] += mem[i][j - 1];
                }
                if (i - 1 >= 0) {
                    mem[i][j] += mem[i - 1][j];
                }
            }
        }
        System.out.println(mem[20][20]);
    }

}
