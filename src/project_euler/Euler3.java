package project_euler;

public class Euler3 {
    public static void main(String[] args) {
        long input = 600_851_475_143L;
        int div = 2;
        while (div * div <= input) {
            while(input % div == 0) {
                input /= div;
            }
            div++;
        }
        System.out.println(input);
    }
}
