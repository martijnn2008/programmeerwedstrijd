package project_euler;

import java.util.*;

public class Euler5 {
    public static void main(String[] args) {
        int[] prime_factors = new int[20];
        for (int i = 1; i <= 20; i++) {
            int[] temp = getPrimeFactors(i);
            for (int j = 0; j < 20; j++) {
                prime_factors[j] = Math.max(prime_factors[j], temp[j]);
            }
        }
        long output = 1;
        for (int j = 0; j < 20; j++) {
            while (prime_factors[j] > 0) {
                output *= (j + 1);
                prime_factors[j]--;
            }
        }
        System.out.println(output);
    }

    public static int[] getPrimeFactors(int n) {
        int[] output = new int[20];
        int div = 2;
        while (div * div <= n) {
            while(n % div == 0) {
                n /= div;
                output[div - 1]++;
            }
            div++;
        }
        output[n - 1]++;
        return output;
    }
}
