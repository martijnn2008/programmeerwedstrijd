package project_euler;

import java.util.*;

public class Euler10 {
    public static void main(String[] args) {
        boolean[] sieve = new boolean[2_000_001];
        Arrays.fill(sieve, true);
        sieve[0] = false;
        sieve[1] = false;

        for (int i = 0; i < sieve.length; i++) {
            if (sieve[i]) {
                for (int k = 2; k * i < sieve.length; k++) {
                    sieve[k * i] = false;
                }
            }
        }

        long sum = 2;
        for (int i = 3; i < sieve.length; i += 2) {
            if (sieve[i]) {
                sum += i;
            }
        }
        System.out.println(sum);
    }
}
