package project_euler;

public class Euler6 {
    public static void main(String[] args) {
        int n = 100;
        long sum_of_squares = n * (n + 1) * (2 * n + 1) / 6;
        long square_of_sum = (n + 1) * (n + 1) * n * n / 4;
        System.out.println(square_of_sum - sum_of_squares);
    }
}
