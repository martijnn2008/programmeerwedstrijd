package project_euler;

public class Euler52 {
    public static void main(String[] args) {
        int i = 0;
        boolean permutation = false;  
        while (!permutation) {
            i++;
            permutation= true;
            for (int m = 2; m <= 6; m++) { 
                permutation &= isPermutation(i, m * i);
            }
        }
        System.out.println(i);
    }

    public static boolean isPermutation(int a, int b) {
        int[] digits = new int[10];
        while(a > 0) {
            digits[a % 10]++;
            a /= 10;
        }
        while(b > 0) {
            digits[b % 10]--;
            b /= 10;
        }
        for (int i = 0; i < 10; i++) {
            if (digits[i] != 0) {
                return false;
            }
        }
        return true;
    }
} 
