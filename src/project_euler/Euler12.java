package project_euler;

public class Euler12 {
    public static void main(String[] args) {
        int i = 0;
        int triangle = 0;
        int nrDivs = 0;
        while (nrDivs <= 500) {
            i++;
            triangle += i;
            nrDivs = nrDivisors(triangle);
        }
        System.out.println(triangle);
    }

    public static int nrDivisors(int n) {
        int output = 0;
        for (int i = 1; i * i <= n; i++) {
            if (n % i == 0) {
                if (n / i != i) {
                    output += 2; 
                } else {
                    output += 1;
                }
            }
        }
        return output;
    }
}
