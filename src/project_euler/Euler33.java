package project_euler;

public class Euler33 {
    static class Fraction {
        int num, denom;
        int orin, orid;

        public Fraction (int num, int denom) {
            this.num = this.orin = num;
            this.denom = this.orid = denom;
            simplify();
        }

        public void simplify() {
            int gcd = gcd(num, denom);
            if (gcd != 0) {
                num /= gcd;
                denom /= gcd;
            }
        }

        private int gcd(int a, int b) {
            while (b != 0) {
                int c = a % b;
                a = b;
                b = c;
            }
            return a;
        }

        public boolean equals(Fraction o) {
            return this.num == o.num && this.denom == o.denom;
        }

        public String toString() {
            return this.orin + " / " + this.orid;
        }

        public void mul(Fraction o) {
            this.num *= o.num;
            this.denom *= o.denom;
            simplify();
        }
    }
    public static void main(String[] args) {
        Fraction solution = new Fraction(1, 1);
        for (int num = 10; num <= 99; num++) {
            for (int denom = 10; denom <= 99; denom++) {
                Fraction frac = new Fraction(num, denom);
                if(frac.num == 1 && frac.denom == 1) {
                    continue;
                }
                boolean found = false;
                if (num % 10 == denom / 10) {
                    Fraction frac1 = new Fraction(num / 10, denom % 10);
                    found |= frac.equals(frac1);
                }
                if (num / 10 == denom / 10) {
                    Fraction frac2 = new Fraction(num % 10, denom / 10);
                    found |= frac.equals(frac2);
                }
                if (found) {
                    solution.mul(frac);
                }
            }
        }
        System.out.println(solution.denom);
    }
}
