package project_euler;

import java.util.*;

public class Euler32 {
    static Set<Integer> set;
    public static void main(String[] args) {
        set = new HashSet<Integer>();
        algorithm("123456789");
        int sum = 0;
        for (Integer n : set) {
           sum += n; 
        }
        System.out.println(sum);
    }

    public static void algorithm(String input) {
        algorithm("", input);
    }

    public static void algorithm(String prefix, String suffix) {
        int n = suffix.length();
        if (n == 0) {
            for (int j = 2; j < 8; j++) {
                for (int i = 1; i < j; i++) {
                    int a = Integer.parseInt(prefix.substring(0, i));
                    int b = Integer.parseInt(prefix.substring(i, j));
                    if (pandigit(a, b, a * b)) {
                        set.add(a * b);
                    }
                }
            }
        } else {
            for (int i = 0; i < n; i++) {
                algorithm(prefix + suffix.charAt(i), suffix.substring(0, i) +
                        suffix.substring(i + 1, n));
            }
        }
    }

    public static boolean pandigit(int a, int b, int c) {
        int[] digits = new int[10];
        addDigits(digits, Integer.toString(a));
        addDigits(digits, Integer.toString(b));
        addDigits(digits, Integer.toString(c));
        if (digits[0] != 0) return false;
        for (int i = 1; i < 10; i++) {
            if (digits[i] != 1) return false;
        }
        return true;
    }

    public static void addDigits(int[] arr, String input) {
        for (int i = 0; i < input.length(); i++) {
            arr[((int) input.charAt(i)) - 48]++;
        }
    }
}
