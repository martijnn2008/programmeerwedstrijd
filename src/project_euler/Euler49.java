package project_euler;

import java.util.*;

public class Euler49 {
    static boolean[] sieve;
    public static void main(String[] args) {
        ArrayList<Integer> primes = getPrimes(1000, 9999);
        for (int i = 0; i < primes.size() - 1; i++) {
            for (int j = i + 1; j < primes.size(); j++) {
                int prime1 = Math.min(primes.get(i), primes.get(j));
                int prime2 = Math.max(primes.get(i), primes.get(j));
                int prime3 = prime2 * 2 - prime1;
                if (prime1 == 1487 && prime2 == 4817 && prime3 == 8147) {
                    continue; // skip the other solution
                }
                if (prime3 > sieve.length || !sieve[prime3]) {
                    continue; // prime3 is not a prime or too big
                }
                if (isPerm(prime1, prime2) && isPerm(prime1, prime3)) {
                    String output = "" + prime1 + prime2 + prime3;
                    System.out.println(output);
                    return;
                }
            }
        }
    }


    public static boolean isPerm(int a, int b) {
        int[] digits = new int[10];
        for (int i = 0; i < 4; i++) {
            digits[a % 10]++;
            digits[b % 10]--;
            a /= 10;
            b /= 10;
        }
        for (int i = 0; i < 10; i++) {
            if (digits[i] != 0) {
                return false;
            }
        }
        return true;
    }

    public static ArrayList<Integer> getPrimes(int lower, int upper) {
        ArrayList<Integer> output = new ArrayList<>();
        sieve = new boolean[upper + 1];
        Arrays.fill(sieve, true);
        sieve[0] = false;
        sieve[1] = false;
        for (int i = 0; i < sieve.length; i++) {
            if (sieve[i]) {
                if (i >= lower) {
                    output.add(i);
                }
                for (int k = 2; k * i < sieve.length; k++) {
                    sieve[i * k] = false;
                }
            }
        }
        return output;
    }
}
